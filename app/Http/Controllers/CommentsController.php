<?php

namespace App\Http\Controllers;

use Auth;
use App\Comment;
use Illuminate\Http\Request;


class CommentsController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'message' => 'required'
        ]);
        $comment = new Comment;
        $comment->text = $request->get('message');
        $comment->post_id = $request->get('post_id');
        $comment->user_id = Auth::user()->id;
        $comment->save();

        return redirect()->back()->with('status', 'Your comment will be added soon');
    }
    public function toggle($id)
    {
        $comment = Comment::find($id);
        $comment->toggleStatus();

        return redirect()->back();
    }
    public function destroy($id)
    {
        Comment::find($id)->remove();
        return redirect()->back();
    }
}
